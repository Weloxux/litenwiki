<?php
$name = 'LitenWiki';        // How your wiki will refer to itself
$mainpage = 'MainPage';     // Landing page when no specific page is requested

$captcha_enabled = false;   // Whether or not captcha is enabled
$captcha_text = '7 - 3 =';  // Captcha question that needs to be answered if captcha is enabled
$captcha_answer = '4';      // Answer to the captcha question

$enabled = false;           // Whether or not the wiki is accessible
?>
